﻿using Arquitetura_Microsservico.Apresentacao.WebApi.Model;
using Arquitetura_Microsservico.Dominio.Entidade.Model;
using Arquitetura_Microsservico.Dominio.Negocio.Contract;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Arquitetura_Microsservico.Apresentacao.WebApi.Model.Exception;
using Arquitetura_Microsservico.Apresentacao.WebApi.Model.Response;

namespace Arquitetura_Microsservico.Apresentacao.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IDomainProduto _domainProduto;
        private IMapper _mapper;

        public ProdutoController(IDomainProduto domainProduto, IMapper mapper)
        {
            _domainProduto = domainProduto;
            _mapper = mapper;
        }

        /// <summary>
        /// API de cadastro
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns>Produto cadastrado</returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("cadastrar")]
        public async Task<ActionResult> Post(ProdutoModelCadastrar produto)
        {
            try
            {
                Produto produtoCadastro = _mapper.Map<ProdutoModelCadastrar, Produto>(produto);
                await _domainProduto.Cadastrar(produtoCadastro);
                
                return CreatedAtAction(nameof(Post), produtoCadastro);
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// API de atualização
        /// </summary>
        /// <param name="produto">Objeto produto</param>
        /// <returns>Produto cadastrado</returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Route("atualizar")]
        public async Task<ActionResult> Put(ProdutoModelAtualizar produto)
        {
            try
            {
                await _domainProduto.Atualizar(_mapper.Map<ProdutoModelAtualizar, Produto>(produto));
                return Ok(ResponseFactory<string>.GerarResponse("Produto atualizado com sucesso"));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }



        /// <summary>
        /// Metodo de listagem
        /// </summary>
        /// <param name="skip">Total de itens a serem pulados/ignorados da lista</param>
        /// <param name="take">Total de itens a serem retornados</param>
        /// <returns>Lista de produtos</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("listar")]
        public async Task<ActionResult> Get(int skip, int take)
        {
            try
            {                
                IEnumerable<ProdutoModel> produtos = _mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoModel>>(await _domainProduto.ListarTodos().Skip(skip)
                                                                                                                                                    .Take(take)
                                                                                                                                                    .OrderBy(f => f.Nome).ToListAsync());
                if(produtos.Count() == 0)
                {
                    return NotFound(ResponseFactory<IEnumerable<ProdutoModel>>.GerarResponse(produtos));
                }

                 return Ok(ResponseFactory<IEnumerable<ProdutoModel>>.GerarResponse(produtos));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }

        /// <summary>
        /// Metodo de listagem
        /// </summary>
        /// <param name="id">Id do Produto</param>        
        /// <returns>Produto cadastrado</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("listar-por-id")]
        public async Task<ActionResult> GetById(int id)
        {
            try
            {
                ProdutoModel produto = _mapper.Map<Produto, ProdutoModel>(await _domainProduto.ObterPorId(id));

                if(produto == null)
                {
                    return NotFound(ResponseFactory<string>.GerarResponse("Produto não encontrado"));
                }

                return Ok(ResponseFactory<ProdutoModel>.GerarResponse(produto));
            }

            catch (Exception ex)
            {
                return BadRequest(ResponseFactory<ExceptionModel>.GerarResponse(ExceptionFactory.GerarExcecao(ex)));
            }
        }
    }
}
