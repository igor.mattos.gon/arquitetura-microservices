﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Arquitetura_Microsservico.Apresentacao.WebApi.Model
{
    [DataContract]
    public class ProdutoModel
    {
        [DataMember(Name ="idProduto")]
        public int IdProduto { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "valor")]
        public decimal Valor { get; set; }

        [DataMember(Name = "dataCadastro")]
        public DateTime DataCadastro { get; set; }
    }

    [DataContract]
    public class ProdutoModelCadastrar
    {
        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "valor")]
        public decimal Valor { get; set; }
    }

    [DataContract]
    public class ProdutoModelAtualizar
    {
        [DataMember(Name = "idProduto")]
        public int IdProduto { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "valor")]
        public decimal Valor { get; set; }
    }
}
