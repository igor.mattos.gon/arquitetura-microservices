﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Arquitetura_Microsservico.Apresentacao.WebApi.Model.Response
{
    /// <summary>
    /// Classe responsável pelo tratamento da resposta devolvida ao cliente
    /// </summary>
    public static class ResponseFactory<T>
    {
        /// <summary>
        /// Método responsável por tratar a resposta ao cliente
        /// </summary>        
        /// <param name="content">Valor da Resposta</param>
        /// <returns>Objeto Response</returns>
        public static ResponseModel<T> GerarResponse(T content)
        {
            ResponseModel<T> response = new ResponseModel<T>()
            {               
                Total = content != null ? ((content.GetType().GetInterface(nameof(IList)) != null) ? (content as IList).Count : 1) : 0,
                Content = content
            };

            return response;
        }
    }

    /// <summary>
    /// Classe de modelo usada para a resposta
    /// </summary>
    [DataContract]
    public class ResponseModel<T>
    {
        
        [DataMember(Name = "total")]
        public int Total { get; set; }

        [DataMember(Name = "content")]
        public T Content { get; set; }        
    }
}

