using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Arquitetura_Microsservico.Dominio.Negocio.Contract;
using Arquitetura_Microsservico.Dominio.Negocio.Service;
using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Contract;
using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Service;
using Arquitetura_Microsservico.Apresentacao.WebApi.Model;
using Arquitetura_Microsservico.Dominio.Entidade.Model;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Arquitetura_Microsservico.Apresentacao.WebApi.Model.Exception;

namespace Arquitetura_Microsservico
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson
                (options =>
                {
                    options.SerializerSettings.DateFormatString = "yyyy-MM-dd hh:mm:ss";
                });

            //Configurar a string de conex�o
            services.AddDbContext<DataContext>(options =>
                                               options.UseLazyLoadingProxies().UseInMemoryDatabase("BD_TESTE_ARQUITETURA"));

            //Inje��o de Depend�ncia (com IoC) do dom�nio            
            services.AddScoped(typeof(IDomainProduto), typeof(DomainProduto));

            //Inje��o de Depend�ncia (com IoC) do reposit�rio
            services.AddScoped(typeof(IRepositoryProduto), typeof(RepositoryProduto));

            //Auto Mapper
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProdutoModelCadastrar, Produto>();
                cfg.CreateMap<ProdutoModelAtualizar, Produto>();
                cfg.CreateMap<Produto, ProdutoModel>();                
            });

            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            //Adicionar o Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Arquitetura_Microsservico", Version = "v1" });
            });

            services.AddRouting(options => options.LowercaseUrls = true);            

            services.AddMvc().ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressMapClientErrors = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {                    
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Arquitetura_Microsservico v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //Configurar o handler para gera��o da exce��o que n�o for status code 400
            app.UseExceptionHandler(a => a.Run(async context =>
            {
                IExceptionHandlerPathFeature exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                Exception exception = exceptionHandlerPathFeature.Error;

                await context.Response.WriteAsJsonAsync(ExceptionFactory.GerarExcecao(exception));
            }));
        }
    }
}
