﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Dominio.Entidade.Model
{
    /// <summary>
    /// Classe Produto
    /// </summary>
    public class Produto
    {
        public Produto()
        {
            DataCadastro = DateTime.Now;
        }

        public int IdProduto { get; set; }

        public string Nome { get; set; }

        public decimal Valor { get; set; }

        public DateTime DataCadastro { get; set; }
    }
}
