# Arquitetura de Micro Serviços

![alt logo1](https://dm2304files.storage.live.com/y4m8cUnCtj_z0aAeorjFrrumVvAVUnN1O8LjHLDlxZU8C5inSMEl1gufwZWeMoRoXWwI_8p-rZTOJIoKmrgTQ7k1WfS08qo-dnyMea_UAZizG2Oc74PM8TXEiIcurHSI5vCHCbLxCyBfZrI7stgFsD5ng1WP1JAcEyBd7Utu1d-2lX3l7QHJdx_MUHXyREhAkgytqhiKP7sVAoUnA1wK1KB1g/arquitetura_geral.png?psid=1&width=513&height=706)

_1.0	Visão geral da arquitetura_


# Camadas

## 0-	Apresentação

Camada onde se localizam os controladores, classes de modelo DTO e arquivos de configuração da aplicação. Essa camada é responsável pelo tratamento e apresentação da informação ao cliente que consumir o micro serviço.

## 1- Domínio

   1-1 Entidades

  Camada responsável pelas classes de modelo e enumerados que irão refletir a estrutura do banco de dados.

   1-1 Negócio

   Camada responsável por armazenar toda e qualquer regra de negócio da aplicação.


## 2-	Infraestrutura

   2-1	Repositório

   Camada responsável pelo acesso ao banco de dados e mapeamento objeto-relacional do framework de persistência.


   2-2	Serviços Externos

   Camada responsável pelo consumo de aplicações de terceiros e/ou outros micro serviços de outros contextos.


## 3-	Teste

  Camada de testes unitários da aplicação, podendo ser estendida a testes de integração, futuramente.


# Bibliotecas

![alt logo1](https://dm2304files.storage.live.com/y4mIM6nE5BumCHQHMNsEpNg5jCzdyppJA6JsA1kIkcNdslHJmfo2glDamkOF4v34XlLDpI0IS6CkcalpCEbx-8IAv7sdj-dUVPN3YZXMKpXdX1Ckmo0cGePlR8Qs_rlU3ESe6gsxG2I_KWrw9OfpEJsFqUMQN4pRiH5a7v37o6AWI7_CBJn0Oq5tT8xV3ChG3gl5QujmYckGPpkWZN7loOyFw/bibliotecas.png?psid=1&width=492&height=228)
 
_1.1	Bibliotecas usadas_



# Tecnologias e Patterns

- Injeção de Dependência
- Inversão de Controle
- AutoMapper
- S.O.L.I.D
- Contexto Delimitado (DDD)
- Teste Unitário (NUnit)
- Swagger
- Docker
- APIs Assíncronas


# Padronização de Formatos


## Tipos de Respostas (Status Code)
- 200 (sucesso)
- 400 (Bad Request)


## Response (JSON)

{
  "sucesso": true para sucesso / false para erro,
  "total": total de itens envolvidos na resposta,
  "valor": objeto de retorno da resposta, podendo ser qualquer coisa, string, lista ou exceção
}


## Exception (Devolvido dentro do atributo valor do response, em caso de erro)

{
    "id": identificador único da exceção,
    "origem": biblioteca de origem,
    "mensagem": mensagem de erro
    "helperLink": link de ajuda, caso exista
}


## Formato de data
dd/MM/yyyy HH:mm:ss (23/12/2020 10:43:03)


## Formato de Paginação
Envio das variáveis skip e take. Ambas inteiras.




