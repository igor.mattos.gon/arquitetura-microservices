﻿using Arquitetura_Microsservico.Dominio.Entidade.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Configuration
{
    public class ProdutoConfiguration : IEntityTypeConfiguration<Produto>
    {
        /// <summary>
        /// Método que mapeia os atributos da classe na tabela
        /// </summary>
        /// <param name="builder">Construtor do Mapeamento</param>
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("PRODUTO");

            builder.HasKey(m => m.IdProduto);

            builder.Property(m => m.IdProduto)
                   .HasColumnName("ID_PRODUTO")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(m => m.Nome)
            .HasColumnName("NOME")
            .HasMaxLength(50)
            .HasColumnType("VARCHAR")
            .IsRequired();

            builder.Property(m => m.Valor)
            .HasColumnName("VALOR")
            .HasColumnType("DECIMAL(8,2)")
            .HasMaxLength(10)
            .IsRequired();

            builder.Property(m => m.DataCadastro)
            .HasColumnName("DTH_CADASTRO")
            .HasColumnType("DATETIME")            
            .IsRequired();
        }
    }
}
