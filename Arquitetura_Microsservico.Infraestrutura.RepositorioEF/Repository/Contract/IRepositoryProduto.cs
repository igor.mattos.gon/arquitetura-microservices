﻿using Arquitetura_Microsservico.Dominio.Entidade.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Contract
{
    public interface IRepositoryProduto : IRepositoryBase<Produto>
    {
    }
}
