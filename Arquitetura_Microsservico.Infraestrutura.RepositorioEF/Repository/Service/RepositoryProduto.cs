﻿using Arquitetura_Microsservico.Dominio.Entidade.Model;
using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Context;
using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Service
{
    public class RepositoryProduto : RepositoryBase<Produto>, IRepositoryProduto
    {
        public RepositoryProduto(DataContext dataContext)
            : base(dataContext)
        {
        }
    }
}
