﻿using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Context
{
    public class DataContext : DbContext
    {

        public DataContext(DbContextOptions<DataContext> options)
                : base(options)
        {

        }

        /// <summary>
        /// Método que adiciona os arquivos de mapeamento de cada classe
        /// </summary>
        /// <param name="modelBuilder">Construtor do Modelo</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProdutoConfiguration());
        }
    }
}
