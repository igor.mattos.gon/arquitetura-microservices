﻿using Arquitetura_Microsservico.Dominio.Entidade.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Dominio.Negocio.Contract
{
    public interface IDomainProduto : IDomainBase<Produto>
    {
    }
}
