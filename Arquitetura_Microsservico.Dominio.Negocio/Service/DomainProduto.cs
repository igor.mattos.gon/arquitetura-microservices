﻿using Arquitetura_Microsservico.Dominio.Entidade.Model;
using Arquitetura_Microsservico.Dominio.Negocio.Contract;
using Arquitetura_Microsservico.Infraestrutura.RepositorioEF.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Arquitetura_Microsservico.Dominio.Negocio.Service
{
    public class DomainProduto : DomainBase<Produto>, IDomainProduto
    {
        private readonly IRepositoryProduto _repositoryProduto;

        public DomainProduto(IRepositoryProduto repositoryProduto)
            : base(repositoryProduto)
        {
            _repositoryProduto = repositoryProduto;
        }
    }
}
